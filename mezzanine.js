var http = require('http');
var url = require('url');
var dt = require('./timemodule.js');
var fs = require('fs');

http.createServer(function (req, res) {
    
    var query = url.parse(req.url, true).query;
    var address = "." + url.parse(req.url, true).pathname === "./" ? "index" : "." + url.parse(req.url, true).pathname;
    console.log(address);
    var history = "<p>" + query.arg + " " + query.param + " " + dt.myDateTime()+ "</p>";

    if(req.url !== '/favicon.ico') {
    	fs.appendFile('index.html', dt.wrapInHeader(history), function(err) {
			if (err) {
				throw err;
			}
			else
			{
				console.log("Saved " + dt.wrapInHeader(history) + " to index.html");
				fs.readFile(address + ".html", function(err, data) {
			    	res.writeHead(200, {'Content-Type': 'text/html'});
			    	if (err) throw err;
			    	res.write(data);
			    	res.end();
			    });
			}
		});
    }

}).listen(8080);